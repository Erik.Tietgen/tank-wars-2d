public class Moveset {
    private Player player;
    private char moveUp;
    private char moveRight;
    private char moveDown;
    private  char moveLeft;
    private char shoot;

    public Moveset(Player player, char moveUp, char moveRight, char moveDown, char moveLeft, char shoot) {
        this.player = player;
        this.moveUp = moveUp;
        this.moveRight = moveRight;
        this.moveDown = moveDown;
        this.moveLeft = moveLeft;
        this.shoot = shoot;
    }

//    Hier werden die Inputs „erkannt“ und über ein „switch case“ der richtigen Aktion zugeordnet.
//    Die Aktion ist dabei eine Funktion bei „Player“, welche den Input „verarbeitet“ (ist die Aktion erlaubt?)

}
