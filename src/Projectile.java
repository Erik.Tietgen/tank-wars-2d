public class Projectile {

    private int speed = 1;
    private int posX = 0;   // X - center point
    private int posY = 0;   // Y - center point
    private final int radius = 5;  // expansion from the center

//    Hier wird, das eigenständige Objekt „Projektile“ erzeugt.
//    Diese erhält als Start die Pos des Panzer (+ Richtung) und
//    kann eigenständig „Arbeiten“.
//    Dabei bewegt sich das Projektil mit Konstanter Geschwindigkeit und prüft, was vor ihm ist.
//    Entsprechend werden dann Aktionen ausgeführt. Die Logik finden im Projektil an sich statt.
//    Für die Darstellung werden die Pos Daten vom Frame angezeigt.

}
