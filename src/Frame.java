import javax.swing.*;
import java.awt.*;

public class Frame extends JFrame {                                                      // like the "OceanAPP"
    private  Battleground battleground; // from Type JPanel (equal to OceanCanvas)


//    Hier werden die Bilder der einzelnen Objekte anhand Ihre Pos daten angezeigt.
//    Es findet keine Logische Verarbeitung statt

    public void GameFrame() {

        int frameWidth = 800;
        int frameHeight = 800;

        Container contentPane = this.getContentPane();

        this.setSize(frameWidth, frameHeight);      // Basic screen settings for the "GameFrame"
        this.setVisible(true);
        this.setTitle("Tank-Wars 2D");
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();    // Place frame in the center of the screen
        int x = (dim.width - frameWidth) / 2;
        int y = (dim.height - frameHeight) / 2;
        this.setLocation(x, y);


        battleground = new Battleground();
        contentPane.add(battleground);
    }

    public Battleground getBattleground() {
        return battleground;
    }
}
