public class Tank {

    private int speed = 1;
    private int orientation = 1; // 1 = up, 2 = right, 3 = down, 4 = left
    private int posX = 0;   // X - center point
    private int posY = 0;   // Y - center point
    private final int radius = 30;  // expansion from the center

    public Tank(int speed, int orientation, int posX, int posY) {
        this.speed = speed;
        this.orientation = orientation;
        this.posX = posX;
        this.posY = posY;
    }

//    Der „Tank“ hat die Aufgabe Daten zu „Speichern / verwalten“,
//    dieser erhält die Pos von dem Spieler.
//    Die Koordinaten, werden dann auf dem Frame angezeigt.
//    Im „Tank“ an sich, werden keine logischen Aktionen ausgeführt / berechnet.

}
